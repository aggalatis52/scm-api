import os
import sys
import time
import threading
import pymysql.cursors
from utils.helpers import Helpers

class MySQL:
    _instance = None
    
    def __new__(self):
        if self._instance is None:
            self._instance = super(MySQL, self).__new__(self)
            self.port = os.getenv("MYSQL_PORT")
            self.host = os.getenv("MYSQL_HOST")
            self.user = os.getenv("MYSQL_USER")
            self.password = os.getenv("MYSQL_PASS")
            self.database = os.getenv("MYSQL_DB")
            self.connection = None
        return self._instance
        

    def connect(self):
        try:
            self.connection = pymysql.connect(host=self.host,
                             user=self.user,
                             password=self.password,
                             database=self.database,
                             port=int(self.port),
                             charset='utf8mb4',
                             cursorclass=pymysql.cursors.DictCursor)
            print("MySQL connected!")
            ping_thread = threading.Thread(target=self.ping_connection)
            ping_thread.start()
        except Exception as e:
            print(e)
            sys.exit()
    
    def query(self, sql, params):
        if self.connection == None:
            print("Query error!")
            return

        with self.connection.cursor() as cursor:
            cursor.execute(sql, params)
            self.connection.commit()
            result = cursor.fetchall()
            return result
    
    def ping_connection(self):
        while True:
            self.connection.ping(reconnect=True)
            time.sleep(60)
        
