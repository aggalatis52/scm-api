# import os
# from fastapi import APIRouter, Response, status
# from sqlalchemy import select, delete
# from models import User
# from config.db import session
# from schemas.user import User as UserSchema
# from schemas.user import AuthUser as AuthUser
# from jose import jwt
# import hashlib


# user = APIRouter()

# @user.get("/api/v1/users")
# async def get_users():
#     stmt = select(User)
#     users = session.scalars(stmt)
#     ret_users = []
#     for user in users:
#         ret_users.append(user.json())
#     return ret_users


# @user.post("/api/v1/users/auth")
# async def auth_user(user: AuthUser, response: Response):
#     try:
#         hashed_pass = hashlib.sha256(user.password.encode('utf-8')).hexdigest()
#         db_user = session.query(User).filter(User.email == user.email).filter(User.password == hashed_pass).first()
#         if db_user == None:
#             response.status_code = status.HTTP_401_UNAUTHORIZED
#             return None
#         token_data = {
#             "name": db_user.name,
#             "email": db_user.email,
#             "role_id": db_user.role_id,
#         }
#         token = jwt.encode(token_data, os.getenv("JWT_SECRET_KEY"), algorithm='HS256')
#         print(db_user)
#         db_user.set_token(token)
#         return db_user
#     except Exception as e:
#         response.status_code = status.HTTP_500_INTERNAL_SERVER_ERROR
#         return {"error": str(e)}


# @user.post("/api/v1/users")
# async def create_user(user: UserSchema, response: Response):
#     try:
#         hashed_pass = hashlib.sha256(user.password.encode('utf-8')).hexdigest()
#         new_user = User(email=user.email, name=user.name,password=hashed_pass, role_id=user.role_id)
#         session.add(new_user)
#         session.commit()
#         session.refresh(new_user)
#         response.status_code = status.HTTP_201_CREATED
#         token_data = {
#             "name": new_user.name,
#             "email": new_user.email,
#             "role_id": new_user.role_id,
#         }
#         token = jwt.encode(token_data, os.getenv("JWT_SECRET_KEY"), algorithm='HS256')
#         new_user.set_token(token)
#         return new_user
#     except Exception as e:
#         session.rollback()
#         response.status_code = status.HTTP_500_INTERNAL_SERVER_ERROR
#         return {"error": str(e)}

# @user.delete("/api/v1/users/{user_id}")
# async def delete_user(user_id: int, response: Response):
#     try:
#         stmt = delete(User).where(User.id == user_id)
#         session.execute(stmt)
#         session.commit()
#         response.status_code = status.HTTP_200_OK
#         return "OK"
#     except Exception as e:
#         session.rollback()
#         response.status_code = status.HTTP_500_INTERNAL_SERVER_ERROR
#         return {"error": str(e)}

# @user.options("/api/v1/users")
# async def return_options(response: Response):
#     response.status_code = status.HTTP_200_OK
#     return

# @user.options("/api/v1/users/auth")
# async def return_options(response: Response):
#     response.status_code = status.HTTP_200_OK
#     return