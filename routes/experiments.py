import tensorflow as tf
from fastapi import APIRouter, Response, status
from utils.schemas import Experiment
from classes.model import Model
from utils.helpers import Helpers

experiments = APIRouter()

@experiments.post("/api/v1/experiments/predict")
async def get_prediction(experiment: Experiment):
    mo = Model()
    experiment_values = Helpers.format_experiment_dictionary_to_tuple(experiment)
    normalized_values = tf.keras.utils.normalize([experiment_values], axis=1)
    classification, prediction = mo.predict_classification(normalized_values)
    return {
        'prediction': str(prediction),
        'classification': str(classification)    
    }

@experiments.options("/api/v1/experiments/predict")
async def handle_options(response: Response):
    response.status_code = status.HTTP_200_OK
    return