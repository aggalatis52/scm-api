
import tensorflow as tf
import numpy as np
from sklearn.model_selection import train_test_split
from utils.dbHelpers import DBHelpers

dbHelp = DBHelpers()
class Model:
    _instance = None
    
    def __new__(self):
        if self._instance is None:            
            self._instance = super(Model, self).__new__(self)
        return self._instance
    
    def init_model(self):
        self.model = None
        self.structure_model()
        self.train_model()
        print("Model is now trained and ready to predict.")
    
    def structure_model(self):
        self.model = tf.keras.Sequential()
        self.model.add(tf.keras.layers.Dense(72, activation="relu"))
        self.model.add(tf.keras.layers.Dense(36, activation="relu"))
        self.model.add(tf.keras.layers.Dense(18, activation="relu"))
        self.model.add(tf.keras.layers.Dense(1, activation='sigmoid'))
        self.model.compile(loss=tf.keras.losses.BinaryCrossentropy(), optimizer=tf.keras.optimizers.Adamax(learning_rate=0.004), metrics=[
            tf.keras.metrics.BinaryAccuracy(),
            tf.keras.metrics.Precision(),
            tf.keras.metrics.Recall()
        ])
    
    def train_model(self):
        trainingData = np.array(dbHelp.get_training_experiments())
        X = trainingData[:, 1:-1]
        Y = trainingData[:, -1]
        X = tf.keras.utils.normalize(X, axis=1)
        X_train, X_val, Y_train, Y_val = train_test_split(X, Y, test_size=0.12)
        self.model.fit(x=X_train, y=Y_train, epochs=100, batch_size=32, validation_data = (X_val, Y_val))
    
    def predict_classification(self, values):
        prediction = self.model.predict(values)
        classification = 1
        if prediction[0][0] < 0.5: classification = 0
        return classification, prediction[0][0]
