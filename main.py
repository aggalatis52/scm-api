from dotenv import load_dotenv
load_dotenv()
from fastapi import FastAPI, Request, Response, status
# from routes.user import  user
from routes.experiments import  experiments
from connectors.mysql import MySQL
from utils.helpers import Helpers
from utils.dbHelpers import DBHelpers
from classes.model import Model


Helpers()
DBHelpers()

mysql = MySQL()
mysql.connect()

mo = Model()
mo.init_model()

app = FastAPI()

@app.middleware("http")
async def add_reponse_headers(request: Request, call_next):
    response = await call_next(request)
    response.headers["Access-Control-Allow-Origin"] = "*"
    response.headers["Access-Control-Allow-Methods"] = "GET, POST, PUT, DELETE, PATCH, OPTIONS"
    return response

# app.include_router(user)
app.include_router(experiments)

