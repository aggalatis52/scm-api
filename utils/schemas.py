from datetime import datetime
from typing import Optional
from pydantic import BaseModel

class User(BaseModel):
    id: Optional[int]
    name: Optional[str]
    email: str
    password: str
    created_at: Optional[datetime]
    updated_at: Optional[datetime]
    last_login: Optional[datetime]
    role_id: int

class AuthUser(BaseModel):
    email: str
    password: str

class Experiment(BaseModel):
    id: Optional[int]
    w_quantity: float
    c_quantity: float
    c_spec_surf: float
    f_quantity: float
    f_D90: float
    f_D70: float
    f_D50: float
    f_D30: float
    f_D10: float
    f_spec_grav: float
    f_sa: float
    fa_quantity: float
    fa_D90: float
    fa_D70: float
    fa_D50: float
    fa_D30: float
    fa_D10: float
    fa_spec_grav: float
    results: Optional[int]
