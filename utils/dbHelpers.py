from connectors.mysql import MySQL

class DBHelpers:
    _instance = None

    
    def __new__(self):
        if self._instance is None:
            self._instance = super(DBHelpers, self).__new__(self)
        return self._instance

    def __init__(self):
        self.DB = MySQL()
    
    def get_training_experiments(self):
        sql = """SELECT 
                    id,
                    w_quantity,
                    c_quantity,
                    c_spec_surf,
                    f_quantity,
                    f_D90,
                    f_D70,
                    f_D50,
                    f_D30,
                    f_D10,
                    f_spec_grav,
                    f_Sa,
                    fa_quantity,
                    fa_D90,
                    fa_D70,
                    fa_D50,
                    fa_D30,
                    fa_D10,
                    fa_spec_grav,
                    results
                FROM experiments WHERE use_for_training = 1"""
        results = self.DB.query(sql, ())
        ret_data = []
        for res in results:
            ret_data.append((
                res["id"],
                res["w_quantity"],
                res["c_quantity"],
                res["c_spec_surf"],
                res["f_quantity"],
                res["f_D90"],
                res["f_D70"],
                res["f_D50"],
                res["f_D30"],
                res["f_D10"],
                res["f_spec_grav"],
                res["f_Sa"],
                res["fa_quantity"],
                res["fa_D90"],
                res["fa_D70"],
                res["fa_D50"],
                res["fa_D30"],
                res["fa_D10"],
                res["fa_spec_grav"],
                res["results"]
            ))
        return ret_data
