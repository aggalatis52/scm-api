
class Helpers:
    _instance = None
    
    def __new__(self):
        if self._instance is None:
            self._instance = super(Helpers, self).__new__(self)
        return self._instance

    def format_experiment_dictionary_to_tuple(experiment):
        return (
            experiment.w_quantity,
            experiment.c_quantity,
            experiment.c_spec_surf,
            experiment.f_quantity,
            experiment.f_D90,
            experiment.f_D70,
            experiment.f_D50,
            experiment.f_D30,
            experiment.f_D10,
            experiment.f_spec_grav,
            experiment.f_sa,
            experiment.fa_quantity,
            experiment.fa_D90,
            experiment.fa_D70,
            experiment.fa_D50,
            experiment.fa_D30,
            experiment.fa_D10,
            experiment.fa_spec_grav
        )

        
